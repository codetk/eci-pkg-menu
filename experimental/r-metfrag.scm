;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Todor Kondić <tk.code@protonmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
(define-module (experimental r-metfrag)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system r)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages mass-spectrometry)
  #:use-module (experimental r-squash)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages)
  #:use-module (srfi srfi-1))

(define-public r-metfrag
  (package
   (name "r-metfrag")
   (version "2.4.2")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url  "https://github.com/MaliRemorker/MetFragR.git")
	   (commit "v2.4.2-guix")))
     (sha256
      (base32
       "0cas0k110hx784ygh1g0l8zhjx3m4wggxj9kq54700p8dvgv29ap"
       ;; "0pxy1hg2wv2v9717hb6aj1y057jw6hp7gh3s1ylif844hixj7sbn"
       ))))
   (properties `((upstream-name . "metfRag")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-rjava" ,r-rjava)
      ("r-cdk" ,r-cdk)
      ("r-cdklibs" ,r-cdklibs)
      ("r-squash" ,r-squash)
      ("r-rjson" ,r-rjson)
      ("r-fingerprint" ,r-fingerprint)))
   (home-page
    "https://github.com/ipb-halle/MetFragR")
   (synopsis
    "R package for MetFrag")
   (description
    "R package for MetFrag")
   (license #f)))			;Which LGPL ?
