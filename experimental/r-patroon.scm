;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Todor Kondić <tk.code@protonmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (experimental r-patroon)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system r)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages bioconductor)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages graph)
  #:use-module (gnu packages)
  #:use-module (experimental r-envipick)
  #:use-module (experimental r-fst)
  #:use-module (experimental r-d3heatmap)
  #:use-module (experimental r-visnetwork)
  #:use-module (experimental r-handsontable)
  #:use-module (experimental r-templates)
  #:use-module (experimental r-camera)
  #:use-module (experimental r-rsm)
  #:use-module (experimental r-ramclustr)
  #:use-module (experimental r-diagrammer)
  #:use-module (gnu packages mass-spectrometry)
  #:use-module (srfi srfi-1))


(define-public r-patroon
  (package
    (name "r-patroon")
    (version "0.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url  "https://github.com/rickhelmus/patRoon")
	     (commit "e2f069162d14804c733692e0796cca812e6051cc")))
       (sha256
        (base32
         "0wccrzlrx73prp5p259n24516gw0nqfdyyxc2yjlhxn8p9gym4mk"))))
    (properties `((upstream-name . "patRoon")))
    (build-system r-build-system)
    (propagated-inputs
     `(("r-checkmate" ,r-checkmate)
       ("r-rcpp" ,r-rcpp)
       ("r-venndiagram" ,r-venndiagram)
       ("r-upsetr" ,r-upsetr)
       ("r-rcolorbrewer" ,r-rcolorbrewer)
       ("r-data-table" ,r-data-table)
       ("r-withr" ,r-withr)
       ("r-envipick" ,r-envipick)
       ("r-xml" ,r-xml)
       ("r-dbi" ,r-dbi)
       ("r-rsqlite" ,r-rsqlite)
       ("r-fst" ,r-fst)
       ("r-processx" ,r-processx)
       ("r-msnbase" ,r-msnbase)
       ("r-xcms" ,r-xcms)
       ("r-cluster" ,r-cluster)
       ("r-fastcluster" ,r-fastcluster)
       ("r-gplots" ,r-gplots)
       ("r-d3heatmap" ,r-d3heatmap)
       ("r-dynamictreecut" ,r-dynamictreecut)
       ("r-dendextend" ,r-dendextend)
       ("r-igraph" ,r-igraph)
       ("r-visnetwork" ,r-visnetwork)
       ("r-rjava" ,r-rjava)
       ("r-cdk" ,r-cdk)
       ("r-fingerprint" ,r-fingerprint)
       ("r-mzr" ,r-mzr)
       ("r-circlize" ,r-circlize)
       ("r-miniui" ,r-miniui)
       ("r-plotly" ,r-plotly)
       ("r-handsontable" ,r-handsontable)
       ("r-rstudioapi" ,r-rstudioapi)
       ("r-htmlwidgets" ,r-htmlwidgets)
       ("r-shiny" ,r-shiny)
       ("r-templates" ,r-templates)
       ("r-camera" ,r-camera)
       ("r-envipat" ,r-envipat)
       ("r-nontarget" ,r-nontarget)
       ("r-knitr" ,r-knitr)
       ("r-markdown" ,r-markdown)
       ("r-magrittr" ,r-magrittr)
       ("r-flexdashboard" ,r-flexdashboard)
       ("r-dt" ,r-dt)
       ("r-kableextra" ,r-kableextra)
       ("r-r-utils" ,r-r-utils)
       ("r-magick" ,r-magick)
       ("r-glue" ,r-glue)
       ("r-ggplot2" ,r-ggplot2)
       ("r-ggrepel" ,r-ggrepel)
       ("r-cowplot" ,r-cowplot)
       ("r-jsonlite" ,r-jsonlite)
       ("r-rdpack" ,r-rdpack)
       ("r-rsm" ,r-rsm)
       ("r-ramclustr" ,r-ramclustr)
       ("r-diagrammer" ,r-diagrammer)))
    (home-page
     "https://github.com/rickhelmus/patRoon")
    (synopsis
     "R package patRoon aims to provide a common interface to
various (primarily open-source) software solutions for mass
spectrometry based non-target analysis.")
    (description
     "R package patRoon aims to provide a common interface to
various (primarily open-source) software solutions for mass
spectrometry based non-target analysis.")
    (license license:gpl3)))
