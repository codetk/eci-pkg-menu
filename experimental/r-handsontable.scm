(define-module (experimental r-handsontable)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system r)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages mass-spectrometry)
  #:use-module (gnu packages chemistry)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages)
  #:use-module (srfi srfi-1))


(define-public r-handsontable
  (package
   (name "r-handsontable")
   (version "0.3.7")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "rhandsontable" version))
     (sha256
      (base32
       "1y6dlpzid5apjx0gphkym7gq3lhwp0bxan8ca274pg170xyb33h8"))))
   (properties `((upstream-name . "rhandsontable")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-htmlwidgets" ,r-htmlwidgets)
      ("r-jsonlite" ,r-jsonlite)
      ("r-magrittr" ,r-magrittr)))
   (home-page
    "http://jrowen.github.io/rhandsontable/")
   (synopsis
    "Interface to the 'Handsontable.js' Library")
   (description
    "An R interface to the 'Handsontable' JavaScript library, which is a minimalist Excel-like data grid editor.  See <https://handsontable.com/> for details.")
   (license license:expat)))

