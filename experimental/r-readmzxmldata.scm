;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Todor Kondić <tk.code@protonmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
(define-module (experimental r-readmzxmldata)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system r)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages)
  #:use-module (srfi srfi-1))

(define-public r-readmzxmldata
  (package
    (name "r-readmzxmldata")
    (version "2.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "readMzXmlData" version))
        (sha256
          (base32
            "03lnhajj75i3imy95n2npr5qpm4birbli922kphj0w3458nq8g8w"))))
    (properties `((upstream-name . "readMzXmlData")))
    (build-system r-build-system)
    (propagated-inputs
      `(("r-base64enc" ,r-base64enc)
        ("r-digest" ,r-digest)
        ("r-xml" ,r-xml)))
    (home-page
      "https://cran.r-project.org/web/packages/readMzXmlData")
    (synopsis
      "Reads Mass Spectrometry Data in mzXML Format")
    (description
      "This package provides functions for reading mass spectrometry data in mzXML format.")
    (license license:gpl3+)))
