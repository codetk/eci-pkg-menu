;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Todor Kondić <tk.code@protonmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
(define-module (experimental r-rsm)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system r)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages)
  #:use-module (srfi srfi-1))


(define-public r-rsm
  (package
   (name "r-rsm")
   (version "2.10")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "rsm" version))
     (sha256
      (base32
       "0a6bxrb0qad40lnigqjv2pnwwzcwi1rcmh9gb1a1b00k1n3mnmlc"))))
   (properties `((upstream-name . "rsm")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-estimability" ,r-estimability)))
   (home-page
    "https://cran.r-project.org/web/packages/rsm")
   (synopsis "Response-Surface Analysis")
   (description
    "This package provides functions to generate response-surface
designs, fit first- and second-order response-surface models, make
surface plots, obtain the path of steepest ascent, and do canonical
analysis.  A good reference on these methods is Chapter 10 of Wu, C-F
J and Hamada, M (2009) \"Experiments: Planning, Analysis, and
Parameter Design Optimization\" ISBN 978-0-471-69946-0.")
   (license license:gpl2+)))

