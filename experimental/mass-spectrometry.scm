;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Todor Kondić <tk.code@protonmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (experimental mass-spectrometry)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system r)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages mass-spectrometry)
  #:use-module (gnu packages chemistry)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages statistics)
  #:use-module (experimental r-handsontable)
  #:use-module (gnu packages)
  #:use-module (srfi srfi-1))


(define-public r-massbank-remorker
  (package
    (name "r-massbank-remorker")
    (version "2.11.3-1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url  "https://github.com/MaliRemorker/RMassBank")
	     (commit "085369d5266fb4ff81b9bb656d44567a3a29a016")))
       (sha256
        (base32
         "04506jdbynf0baix9wdi6kcs4iiki5pf7fml8cbv4piwqc1s8j4q"))))
    (properties `((upstream-name . "RMassBankMR")))
    (build-system r-build-system)
    (inputs `(("openbabel" ,openbabel)))
    (propagated-inputs
     `(("r-biobase" ,r-biobase)
       ("r-digest" ,r-digest)
       ("r-httr" ,r-httr)
       ("r-msnbase" ,r-msnbase)
       ("r-mzr" ,r-mzr)
       ("r-cdk" ,r-cdk)
       ("r-rcpp" ,r-rcpp)
       ("r-rcurl" ,r-rcurl)
       ("r-rjson" ,r-rjson)
       ("r-s4vectors" ,r-s4vectors)
       ("r-xml" ,r-xml)
       ("r-markdown" ,r-markdown)
       ("r-knitr" ,r-knitr)
       ("r-yaml" ,r-yaml)))
    (home-page
     "https://bioconductor.org/packages/release/bioc/html/RMassBank.html")
    (synopsis
     "Workflow to process tandem MS files and build MassBank records")
    (description
     "Workflow to process tandem MS files and build MassBank records.
Functions include automated extraction of tandem MS spectra, formula
assignment to tandem MS fragments, recalibration of tandem MS spectra with
assigned fragments, spectrum cleanup, automated retrieval of compound
information from Internet databases, and export to MassBank records.
     WARNING: This is MaliRemorker's experimental version.")
    (license license:artistic2.0)))


(define-public r-shinyscreen
  (package
    (name "r-shinyscreen")
    (version "0.8.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url  "https://git-r3lab.uni.lu/eci/shinyscreen.git")
	     (commit "v0.8.0")))
       (sha256
        (base32
         "0i4gnkhcw18a01rlj1ksi8461k0wmr2pppmhb7imyki5821as1w2"))))
    (properties `((upstream-name . "shinyscreen")))
    (build-system r-build-system)
    (inputs
     `(("r-scales" ,r-scales)
       ("r-yaml" ,r-yaml)
       ("r-mzr" ,r-mzr)
       ("r-msnbase" ,r-msnbase)
       ("r-gridextra" ,r-gridextra)
       ("r-ggplot2" ,r-ggplot2)
       ("r-cowplot" ,r-cowplot)
       ("r-colorbrewer" ,r-rcolorbrewer)
       ("r-curl" ,r-curl)
       ("r-shiny" ,r-shiny)
       ("r-shinydashboard" ,r-shinydashboard)
       ("r-shinyfiles" ,r-shinyfiles)
       ("r-handsontable" ,r-handsontable)
       ("r-chemmass" ,r-chemmass)))
    (home-page
     "https://git-r3lab.uni.lu/eci/shinyscreen")
    (synopsis
     "Mass-spectrometry prescreening application")
    (description
     "Takes mzML files containing the mass spectrometry data and a
list of SMILES, or masses (if compounds are unknown) and plots the
chromatogram per compound as well as the MS2 spectra of its
fragments.")
    (license license:artistic2.0)))


(define-public r-shinyscreen-next
  (package
    (name "r-shinyscreen-next")
    (version "0.7.5")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url  "https://git-r3lab.uni.lu/eci/shinyscreen.git")
	     (commit "v0.7.5")))
       (sha256
        (base32
	 "0fflbsmg3m1s5h17dsyrbb24k8mncp4al6i2n9rrg7yybpg28qfw"))))
    (properties `((upstream-name . "shinyscreen")))
    (build-system r-build-system)
    (inputs
     `(("r-scales" ,r-scales)
       ("r-yaml" ,r-yaml)
       ("r-mzr" ,r-mzr)
       ("r-msnbase" ,r-msnbase)
       ("r-gridextra" ,r-gridextra)
       ("r-ggplot2" ,r-ggplot2)
       ("r-cowplot" ,r-cowplot)
       ("r-colorbrewer" ,r-rcolorbrewer)
       ("r-curl" ,r-curl)
       ("r-shiny" ,r-shiny)
       ("r-shinydashboard" ,r-shinydashboard)
       ("r-shinyfiles" ,r-shinyfiles)
       ("r-handsontable" ,r-handsontable)
       ("r-chemmass" ,r-chemmass)))
    (home-page
     "https://git-r3lab.uni.lu/eci/shinyscreen")
    (synopsis
     "Mass-spectrometry prescreening application")
    (description
     "Takes mzML files containing the mass spectrometry data and a
list of SMILES, or masses (if compounds are unknown) and plots the
chromatogram per compound as well as the MS2 spectra of its
fragments.")
    (license license:artistic2.0)))







