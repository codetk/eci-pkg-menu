;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Todor Kondić <tk.code@protonmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (experimental r-templates)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system r)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages)
  #:use-module (srfi srfi-1))
(define-public r-aoos
  (package
    (name "r-aoos")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "aoos" version))
        (sha256
          (base32
            "0y92vs27i0mkpjdclqzq4j9g1axkymhi3v8xp1v6hazh35yzjkfj"))))
    (properties `((upstream-name . "aoos")))
    (build-system r-build-system)
    (propagated-inputs
      `(("r-magrittr" ,r-magrittr)
        ("r-roxygen2" ,r-roxygen2)))
    (home-page "https://wahani.github.io/aoos")
    (synopsis "Another Object Orientation System")
    (description
      "Another implementation of object-orientation in R.  It provides
syntactic sugar for the S4 class system and two alternative new
implementations.  One is an experimental version built around S4 and
the other one makes it more convenient to work with lists as
objects.")
    (license expat)))

(define-public r-dat
  (package
    (name "r-dat")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "dat" version))
        (sha256
          (base32
            "1nw16hsw9b4hpcl5rqrlw6yk9y2h1pd0ra1c7iz82sknkyd5afi6"))))
    (properties `((upstream-name . "dat")))
    (build-system r-build-system)
    (inputs
     `(("r-knitr" ,r-knitr)
       ("r-markdonw" ,r-markdown)))
    (propagated-inputs
      `(("r-aoos" ,r-aoos)
        ("r-data-table" ,r-data-table)
        ("r-dplyr" ,r-dplyr)
        ("r-formula" ,r-formula)
        ("r-magrittr" ,r-magrittr)
        ("r-progress" ,r-progress)
        ("r-tibble" ,r-tibble)))
    (home-page
      "https://cran.r-project.org/web/packages/dat")
    (synopsis "Tools for Data Manipulation")
    (description
      "An implementation of common higher order functions with
syntactic sugar for anonymous function.  Provides also a link to
'dplyr' for common transformations on data frames to work around non
standard evaluation by default.")
    (license expat)))

(define-public r-templates
  (package
    (name "r-templates")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "templates" version))
        (sha256
          (base32
            "0xnzj3cvhw1r7zfqaijbfdyclka61jwixcdh05a5z9qwgv0076sh"))))
    (properties `((upstream-name . "templates")))
    (build-system r-build-system)
    (propagated-inputs
      `(("r-dat" ,r-dat)
        ("r-magrittr" ,r-magrittr)
        ("r-stringr" ,r-stringr)))
    (home-page
      "https://cran.r-project.org/web/packages/templates")
    (synopsis "A System for Working with Templates")
    (description
      "This package provides tools to work with template code and text
in R.  It aims to provide a simple substitution mechanism for
R-expressions inside these templates.  Templates can be written in
other languages like 'SQL', can simply be represented by characters in
R, or can themselves be R-expressions or functions.")
    (license expat)))

