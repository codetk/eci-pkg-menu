;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Todor Kondić <tk.code@protonmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
(define-module (experimental r-fst)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system r)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages)
  #:use-module (srfi srfi-1))

(define-public r-fst
  (package
    (name "r-fst")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "fst" version))
        (sha256
          (base32
            "0ya5lh8p0qy5pcr6wxsrn43wj1r9gb9qycyn8gvl26ic3hxwk2rf"))))
    (properties `((upstream-name . "fst")))
    (build-system r-build-system)
    (propagated-inputs `(("r-rcpp" ,r-rcpp)))
    (home-page "https://fstpackage.github.io")
    (synopsis
      "Lightning Fast Serialization of Data Frames for R")
    (description
      "Multithreaded serialization of compressed data frames using the
'fst' format.  The 'fst' format allows for random access of stored
data and compression with the LZ4 and ZSTD compressors created by Yann
Collet.  The ZSTD compression library is owned by Facebook Inc.")
    (license license:agpl3)))

