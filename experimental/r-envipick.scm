;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Todor Kondić <tk.code@protonmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
(define-module (experimental r-envipick)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system r)
  #:use-module (experimental r-readmzxmldata)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages)
  #:use-module (srfi srfi-1))

(define-public r-envipick
  (package
    (name "r-envipick")
    (version "1.5")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "enviPick" version))
        (sha256
          (base32
            "04q6zwqq2ip8b8h2n1jpgx1bzcvi7lazljs0806wiakbc79x232p"))))
    (properties `((upstream-name . "enviPick")))
    (build-system r-build-system)
    (propagated-inputs
      `(("r-readmzxmldata" ,r-readmzxmldata)
        ("r-shiny" ,r-shiny)))
    (home-page
      "https://cran.r-project.org/web/packages/enviPick")
    (synopsis
      "Peak Picking for High Resolution Mass Spectrometry Data")
    (description
      "Sequential partitioning, clustering and peak detection of
centroided LC-MS mass spectrometry data (.mzXML).  Interactive result
and raw data plot.")
    (license license:gpl2+)))

