(define-module (experimental r-ramclustr)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system r)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages mass-spectrometry)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages)
  #:use-module (srfi srfi-1))
(define-public r-webchem
  (package
    (name "r-webchem")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "webchem" version))
        (sha256
          (base32
            "06vnwvh054cj1hflip108h99ps0cb57cq4f8ydrjcxgpplbx0v4w"))))
    (properties `((upstream-name . "webchem")))
    (build-system r-build-system)
    (propagated-inputs
      `(("r-dplyr" ,r-dplyr)
        ("r-httr" ,r-httr)
        ("r-jsonlite" ,r-jsonlite)
        ("r-purrr" ,r-purrr)
        ("r-rcurl" ,r-rcurl)
        ("r-rvest" ,r-rvest)
        ("r-stringr" ,r-stringr)
        ("r-xml2" ,r-xml2)))
    (home-page "https://docs.ropensci.org/webchem")
    (synopsis "Chemical Information from the Web")
    (description
      "Chemical information from around the web.  This package interacts with a suite of web APIs for chemical information.")
    (license license:expat)))

(define-public r-rdisop
  (package
    (name "r-rdisop")
    (version "1.46.0")
    (source
      (origin
        (method url-fetch)
        (uri (bioconductor-uri "Rdisop" version))
        (sha256
          (base32
            "0j8gmgkab3njgf4w1vymx96s8pszh5s1zjxn3g07sgj8w883z5v7"))))
    (properties `((upstream-name . "Rdisop")))
    (build-system r-build-system)
    (propagated-inputs `(("r-rcpp" ,r-rcpp)))
    (home-page "https://github.com/sneumann/Rdisop")
    (synopsis "Decomposition of Isotopic Patterns")
    (description
      "Identification of metabolites using high precision mass
spectrometry.  MS Peaks are used to derive a ranked list of sum
formulae, alternatively for a given sum formula the theoretical
isotope distribution can be calculated to search in MS peak lists.")
    (license license:gpl2)))

(define-public r-interpretmsspectrum
  (package
    (name "r-interpretmsspectrum")
    (version "1.2")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "InterpretMSSpectrum" version))
        (sha256
          (base32
            "1iaw4i14l2vp1f163g2q4pwv9cnqrx14ik4s8zw0iha4zwcrc3ja"))))
    (properties
      `((upstream-name . "InterpretMSSpectrum")))
    (build-system r-build-system)
    (propagated-inputs
      `(("r-dbi" ,r-dbi)
        ("r-doparallel" ,r-doparallel)
        ("r-envipat" ,r-envipat)
        ("r-foreach" ,r-foreach)
        ("r-plyr" ,r-plyr)
        ("r-rdisop" ,r-rdisop)
        ("r-rsqlite" ,r-rsqlite)))
    (home-page
      "http://dx.doi.org/10.1021/acs.analchem.6b02743")
    (synopsis
      "Interpreting High Resolution Mass Spectra")
    (description
      "Annotate and interpret deconvoluted mass
spectra (mass*intensity pairs) from high resolution mass spectrometry
devices.")
    (license license:gpl3)))

(define-public r-ramclustr
  (package
    (name "r-ramclustr")
    (version "1.0.9")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "RAMClustR" version))
        (sha256
          (base32
            "0fw8gps8lyjffrqklfk8ps4fn8ail2m6b54q3805pcw27mb9jxml"))))
    (properties `((upstream-name . "RAMClustR")))
    (build-system r-build-system)
    (propagated-inputs
      `(("r-biocmanager" ,r-biocmanager)
        ("r-dynamictreecut" ,r-dynamictreecut)
        ("r-e1071" ,r-e1071)
        ("r-fastcluster" ,r-fastcluster)
        ("r-ff" ,r-ff)
        ("r-gplots" ,r-gplots)
        ("r-httr" ,r-httr)
        ("r-interpretmsspectrum" ,r-interpretmsspectrum)
        ("r-jsonlite" ,r-jsonlite)
        ("r-pcamethods" ,r-pcamethods)
        ("r-preprocesscore" ,r-preprocesscore)
        ("r-rcurl" ,r-rcurl)
        ("r-stringi" ,r-stringi)
        ("r-stringr" ,r-stringr)
        ("r-webchem" ,r-webchem)
        ("r-xml2" ,r-xml2)))
    (home-page
      "https://github.com/cbroeckl/RAMClustR")
    (synopsis
      "Mass Spectrometry Metabolomics Feature Clustering and Interpretation")
    (description
      "This package provides a feature clustering algorithm for
non-targeted mass spectrometric metabolomics data.  This method is
compatible with gas and liquid chromatography coupled mass
spectrometry, including indiscriminant tandem mass spectrometry <DOI:
10.1021/ac501530d> data.")
    (license license:gpl2+)))

