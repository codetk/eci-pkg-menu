;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Todor Kondić <tk.code@protonmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
(define-module (experimental r-squash)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system r)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages)
  #:use-module (srfi srfi-1))

(define-public r-squash
  (package
    (name "r-squash")
    (version "1.0.9")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "squash" version))
        (sha256
          (base32
            "1x66iamj5pir6l9aqx4x7xylxxjpjrw8vcix9rbhfd0y0y2iqf7z"))))
    (properties `((upstream-name . "squash")))
    (build-system r-build-system)
    (home-page
      "https://github.com/aroneklund/squash")
    (synopsis
      "Color-Based Plots for Multivariate Visualization")
    (description
      "This package provides functions for color-based visualization
of multivariate data, i.e.  colorgrams or heatmaps.  Lower-level
functions map numeric values to colors, display a matrix as an array
of colors, and draw color keys.  Higher-level plotting functions
generate a bivariate histogram, a dendrogram aligned with a
color-coded matrix, a triangular distance matrix, and more.")
    (license license:artistic2.0)))

