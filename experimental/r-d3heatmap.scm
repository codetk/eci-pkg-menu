;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Todor Kondić <tk.code@protonmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
(define-module (experimental r-d3heatmap)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system r)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages)
  #:use-module (srfi srfi-1))


(define-public r-d3heatmap
  (package
    (name "r-d3heatmap")
    (version "0.6.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "d3heatmap" version))
        (sha256
          (base32
            "1ici8j0wzzklhmw94qlxm292qs562vc32wq8mnjsas2n1p35vkmk"))))
    (properties `((upstream-name . "d3heatmap")))
    (build-system r-build-system)
    (inputs
     `(("r-knitr" ,r-knitr)
       ("r-rmarkdown" ,r-rmarkdown)))
    (propagated-inputs
      `(("r-base64enc" ,r-base64enc)
        ("r-dendextend" ,r-dendextend)
        ("r-htmlwidgets" ,r-htmlwidgets)
        ("r-png" ,r-png)
        ("r-scales" ,r-scales)))
    (home-page
      "https://github.com/rstudio/d3heatmap")
    (synopsis
      "Interactive Heat Maps Using 'htmlwidgets' and 'D3.js'")
    (description
      "Create interactive heat maps that are usable from the R
console, in the 'RStudio' viewer pane, in 'R Markdown' documents, and
in 'Shiny' apps.  Hover the mouse pointer over a cell to show details,
drag a rectangle to zoom, and click row/column labels to highlight.")
    (license license:gpl3)))

