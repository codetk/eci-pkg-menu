;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Todor Kondić <tk.code@protonmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gnu packages virtualbox)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages gcc)
  #:use-module (guix build-system trivial)
  #:use-module (guix build-system gnu)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix svn-download)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:use-module (srfi srfi-1))


(define-public kbuild
  (package
    (name "kbuild")
    (version "0.1.9998")
    (source (origin
              (method svn-fetch)
              (uri (svn-reference
                    (url "https://svn.netlabs.org/repos/kbuild/trunk")
                    (revision 3296)))
              (sha256
               (base32 "119rgxbpjkammirhc8g2gs1vi06lckxd56py0v31ynpvjzpm4dzw"))
              (modules '((guix build utils)))
              (snippet                  ;No need for precombiled binaries, we
                                        ;bootstrap ourselves.
               '(begin
                  (delete-file-recursively "kBuild/bin")
                  #t))))
    (build-system trivial-build-system)
    (native-inputs `(("bash" ,bash-minimal)
                     ("coreutils" ,coreutils)
                     ("binutils" ,binutils)
                     ("autoconf" ,autoconf)
                     ("automake" ,automake)
                     ("libtool" ,libtool)
                     ("make" ,gnu-make)
                     ("sed" ,sed)
                     ("flex" ,flex)
                     ("bison" ,bison)
                     ("grep" ,grep)
                     ("gawk" ,gawk)
                     ("gettext-minimal" ,gettext-minimal)
                     ("gcc-progs" ,gcc "out")
                     ("gcc-libs" ,gcc "lib")
                     ("gcc-toolchain" ,gcc-toolchain)
                     ;; ("glibc" ,glibc)
                     ("tar" ,tar)
                     ("xz" ,xz)
                     ("findutils" ,findutils)
                     ("diffutils" ,diffutils)
                     ("texinfo" ,texinfo)
                     ("subversion" ,subversion)))
    ;; (inputs
    ;;  `(("gcc-lib" ,gcc "lib")))
    (arguments
     `(#:modules ((guix build utils))
       #:builder (begin
                   (use-modules (guix build utils))
                   (use-modules (ice-9 popen))
                   (use-modules (ice-9 rdelim))
                   (use-modules (ice-9 regex))
                   (let*
                       ((source (assoc-ref %build-inputs "source"))
                        (out (assoc-ref %outputs "out"))
                        (out-bin (string-append out "/bin"))
                        (local-src-tar "local-src.tar")
                        (local-src-tar-xz (string-append local-src-tar ".xz"))
                        (build-src (string-append (getcwd) "/" "svn-checkout"))
                        (path-string (string-join
                                      (map (lambda (inp) (string-append (cdr inp) "/bin"))
                                           (filter (lambda (x) (not (equal? (car x) "source")))
                                                   %build-inputs))
                                      ":"))
                        (cval (number->string (parallel-job-count)))
                         
                        (kbuild-path (string-append build-src "/kBuild"))
                        (kbuild-env (string-append kbuild-path "/env.sh"))
                        (cubin (string-append (assoc-ref %build-inputs "coreutils") "/bin"))
                        (sh-prog (string-append (assoc-ref %build-inputs "bash") "/bin/sh"))
                        (make-prog (string-append (assoc-ref %build-inputs "make") "/bin/make"))
                        (tar-prog (string-append (assoc-ref %build-inputs "tar") "/bin/tar"))
                        (unxz-prog (string-append (assoc-ref %build-inputs "xz") "/bin/unxz"))
                        (pwd-prog (string-append cubin "/pwd"))
                        (uname-prog (string-append cubin "/uname"))
                        (sleep-prog (string-append cubin "/sleep"))
                        (mkdir-prog (string-append cubin "/mkdir"))
                        (gcc-lib (string-append (assoc-ref %build-inputs "gcc-libs")
                                                "/lib"))
                        (gcc-t-lib (string-append (assoc-ref %build-inputs "gcc-toolchain")
                                                  "/lib"))
                        (gcc-inc (string-append (assoc-ref %build-inputs "gcc-libs")
                                                "/include"))
                        (gcc-t-inc (string-append (assoc-ref %build-inputs "gcc-toolchain")
                                                  "/include"))
                        (gcc-t-linux-inc (string-append (assoc-ref %build-inputs "gcc-toolchain")
                                                        "/include/linux"))
                        (gcc-dir (assoc-ref %build-inputs "gcc-progs"))
                        (cc-prog (string-append (assoc-ref %build-inputs "gcc-progs") "/bin/gcc"))
                        (cxx-prog (string-append (assoc-ref %build-inputs "gcc-progs") "/bin/g++"))
                        (build-cmd `(,kbuild-env "--full" ,make-prog "-f" "bootstrap.gmk" "-j" ,cval)))
                     (copy-file source local-src-tar-xz)
                     (invoke unxz-prog local-src-tar-xz)
                     (invoke tar-prog "-xf" local-src-tar)
                     (setenv "PATH" (string-append path-string ":" (getenv "PATH")))
                     (setenv "KBUILD_PATH" kbuild-path)
                     (setenv "GCC_EXEC_PREFIX" gcc-dir)
                     (let*
                         ((ver (with-input-from-port (open-pipe "gcc -dumpversion" OPEN_READ)
                                 (lambda () (read-line))))
                          (cc1-path (string-append gcc-dir "/libexec/gcc/x86_64-unknown-linux-gnu/" ver))
                          (gcc-lib-x86_64 (string-append gcc-lib "/gcc/x86_64-unknown-linux-gnu/" ver))
                          (lib-path (string-join `(,gcc-lib ,gcc-t-lib ,gcc-lib-x86_64) ":")))
                       (setenv "COMPILER_PATH" cc1-path)
                       (setenv "LIBRARY_PATH" lib-path)
                       (invoke "ls" "-l" gcc-lib))
                     (substitute* kbuild-env
                       (("/bin/sh") sh-prog))
                     (with-directory-excursion build-src
                       ;; The standard building process enters a shell with
                       ;; necessary environment variables defined. This is not
                       ;; very convenient in the case where a build process is
                       ;; controlled by a guix, so it makes sense to extract
                       ;; the environment variables set by the shell, and set
                       ;; them directly from here.
                       (let* ((regp (make-regexp "([^=]+)=(.*)"))
                              (get-env-var (lambda (line)
                                             (let
                                                 ((m (regexp-exec regp line)))
                                               (if m
                                                   (cons
                                                    (match:substring m 1)
                                                    (match:substring m 2))
                                                   (cons "" ""))))))
                         (with-input-from-port (open-pipe* OPEN_READ kbuild-env "--full" "--quiet" "--set")
                           (lambda ()
                             (let loop ((line (read-line)))
                               (if (not (eof-object? line))
                                   (let
                                       ((entry (get-env-var line)))
                                     (setenv (car entry) (cdr entry))
                                     (loop (read-line)))
                                   #t)))))
                       (let*
                           ;; Here we build units needed for bootstraping.
                           ((kbuild-src-kmk (string-append build-src "/src/kmk"))
                            (kbuild-src-kmk-obj (string-append kbuild-src-kmk "/obj"))
                            (kbuild-src-sed (string-append build-src "/src/sed"))
                            (kbuild-src-ash (string-append build-src "/src/ash"))
                            (kbuild-src-deppre (string-append build-src "/src/kDepPre")))
                         (with-directory-excursion kbuild-src-kmk
                           (invoke "autoreconf" "-i")
                           (substitute* "configure"
                             (("/bin/sh") sh-prog)
                             (("/lib/cpp") "cpp"))
                           (mkdir-p kbuild-src-kmk-obj)
                           (map (lambda (fn)
                                  (substitute* fn
                                    (("#!.*/bin/sh")
                                     (string-append "#! " sh-prog))))
                                (find-files "config"))
                           (with-directory-excursion kbuild-src-kmk-obj
                             (invoke "../configure"
                                     (format #f "CFLAGS= -I~a -I~a" gcc-t-inc gcc-t-linux-inc)
                                     ;; (format #f "CC=~a" cc-prog)
                                     ;; (format #f "CXX=~a" cxx-prog)
                                     ;; (format #f "LDFLAGS=-L~a -Wl,-rpath,~a" gcc-lib gcc-lib)
                                     ;; (format #f "GCC_EXEC_PREFIX=~a" gcc-dir)
                                     ;; (format #f "CFLAGS=-B ~a" gcc-dir)
                                     )
                             
                             )))
                       (error "ENOUGH")))
                   #t)))
    (synopsis "Build system for VirtualBox")
    (description "Build system for VirtualBox")
    (home-page "https://trac.netlabs.org/kbuild/wiki")
    (license license:gpl3)))

(define (populate-environment)
  (let* ((kbuild-env (string-append (getcwd) "/kBuild/env.sh"))
	 (regp (make-regexp "([^=]+)=(.*)"))
	 (get-env-var (lambda (line)
			(let
			    ((m (regexp-exec regp line)))
			  (if m
			      (cons
			       (match:substring m 1)
			       (match:substring m 2))
			      (cons "" ""))))))
    (with-input-from-port (open-pipe* OPEN_READ kbuild-env "--full" "--quiet" "--set")
      (lambda ()
	(let loop ((line (read-line)))
	  (if (not (eof-object? line))
	      (let
		  ((entry (get-env-var line)))
		(setenv (car entry) (cdr entry))
		(loop (read-line)))
	      #t))))))


(define-public kbuild-boot-kmk
  (package
    (name "kbuild-boot-kmk")
    (version "0.1.9998")
    (source (origin
              (method svn-fetch)
              (uri (svn-reference
                    (url "https://svn.netlabs.org/repos/kbuild/trunk")
                    (revision 3296)))
              (sha256
               (base32 "119rgxbpjkammirhc8g2gs1vi06lckxd56py0v31ynpvjzpm4dzw"))
              (modules '((guix build utils)
                         (ice-9 ftw)))
              (snippet                  
               '(begin
                  (let ((srcdir (getcwd)))
                    (mkdir-p "../PRESERVE")
                    (mkdir-p "../PRESERVE/kmk")
                    (mkdir-p "../PRESERVE/kBuild")
                    (copy-recursively "src/kmk" "../PRESERVE/kmk")
                    (copy-file "kBuild/env.sh" "../PRESERVE/kBuild/env.sh")
                    (delete-file-recursively srcdir)
                    (copy-recursively "../PRESERVE/kmk" srcdir)
                    (mkdir-p (string-append srcdir "/kBuild"))
                    (copy-file "../PRESERVE/kBuild/env.sh" (string-append srcdir "/kBuild/env.sh"))
                    (delete-file-recursively "../PRESERVE")
                    #t)))))
    (build-system gnu-build-system)
    (native-inputs `(("autoconf" ,autoconf-wrapper)
                     ("gettext-minimal" ,gettext-minimal)
                     ("automake" ,automake)
                     ("coreutils" ,coreutils)
                     ("binutils" ,binutils)))
    (arguments
     `(#:tests? #f
       #:phases (begin
                  (use-modules (guix build utils))
                  (let ((keys %standard-phases)
                        (src-dir (assoc-ref %build-inputs "source"))
                        (build-dir (getcwd))
                        ())
                    (modify-phases
                        (filter (lambda (x)
                                  (memq (car x) keys))
                                %standard-phases)
                      (add-after 'unpack 'testme (lambda _
						   (populate-environment)
						   (invoke "printenv")
                                                  #t)))))))

    
    (synopsis "Build system for VirtualBox. Bootstrap kmk.")
    (description "Build system for VirtualBox")
    (home-page "https://trac.netlabs.org/kbuild/wiki")
    (license license:gpl3)))
