;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Todor Kondić <tk.code@protonmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gnu packages mass-spectrometry)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system r)
  #:use-module (gnu packages chemistry)
  #:use-module (gnu packages rsvg)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages web)
  #:use-module (gnu packages image)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages)
  #:use-module (srfi srfi-1))

(define-public r-fingerprint
  (package
    (name "r-fingerprint")
    (version "3.5.7")
    (source
     (origin
       (method url-fetch)
       (uri (cran-uri "fingerprint" version))
       (sha256
        (base32
         "04jcwkydjrs31pia6kq8z2n9s54im950q08hs2ay15xjxxkmb8ic"))))
    (build-system r-build-system)
    (home-page
     "https://cran.r-project.org/web/packages/fingerprint")
    (synopsis
     "Functions to Operate on Binary Fingerprint Data")
    (description
     "Functions to manipulate binary fingerprints of arbitrary length.  A
fingerprint is represented by an object of S4 class 'fingerprint' which is
internally represented a vector of integers, such that each element represents
the position in the fingerprint that is set to 1.  The bitwise logical
functions in R are overridden so that they can be used directly with
'fingerprint' objects.  A number of distance metrics are also available (many
contributed by Michael Fadock).  Fingerprints can be converted to Euclidean
vectors (i.e., points on the unit hypersphere) and can also be folded using
OR.  Arbitrary fingerprint formats can be handled via line handlers.
Currently handlers are provided for CDK, MOE and BCI fingerprint data.")
    (license (list license:gpl2+ license:gpl3+))))

(define-public r-cdklibs
  (package
    (name "r-cdklibs")
    (version "2.0")
    (source
     (origin
       (method url-fetch)
       (uri (cran-uri "rcdklibs" version))
       (sha256
        (base32
         "05g0y00bw1bqykfbwn4q42krwcfl50jcavaw7yyw9a5m34hvw0l8"))))
    (build-system r-build-system)
    (propagated-inputs `(("r-rjava" ,r-rjava)))
    (home-page
     "https://cran.r-project.org/web/packages/rcdklibs")
    (synopsis "The CDK Libraries Packaged for R")
    (description
     "An R interface to the Chemistry Development Kit, a Java library for
chemoinformatics.  Given the size of the library itself, this package is not
expected to change very frequently.  To make use of the CDK within R, it is
suggested that you use the 'rcdk' package.  Note that it is possible to
directly interact with the CDK using 'rJava'.  However 'rcdk' exposes
functionality in a more idiomatic way.  The CDK library itself is released as
LGPL and the sources can be obtained from <https://github.com/cdk/cdk>.")
    (license (list license:lgpl2.0 license:lgpl2.1 license:lgpl3))))

(define-public r-cdk
  (package
    (name "r-cdk")
    (version "3.4.3")
    (source
     (origin
       (method url-fetch)
       (uri (cran-uri "rcdk" version))
       (sha256
        (base32
         "055k6p40ai2dfc3kn6m1pgj6wryyh3l84w1wy86ckqny24h4xvqd"))))
    (build-system r-build-system)
    (propagated-inputs
     `(("r-fingerprint" ,r-fingerprint)
       ("r-iterators" ,r-iterators)
       ("r-itertools" ,r-itertools)
       ("r-png" ,r-png)
       ("r-cdklibs" ,r-cdklibs)
       ("r-rjava" ,r-rjava)
       ("r-knitr" ,r-knitr)))
    (home-page
     "https://cran.r-project.org/web/packages/rcdk")
    (synopsis "Interface to the 'CDK' Libraries")
    (description
     "Allows the user to access functionality in the 'CDK', a Java framework
for chemoinformatics.  This allows the user to load molecules, evaluate
fingerprints, calculate molecular descriptors and so on.  In addition, the
'CDK' API allows the user to view structures in 2D.")
    (license (list license:lgpl2.0 license:lgpl2.1 license:lgpl3))))

(define-public r-massbank-2.14.1
  (package
    (name "r-massbank")
    (version "2.14.1")
    (source
     (origin
       (method url-fetch)
       (uri (bioconductor-uri "RMassBank" version))
       (sha256
        (base32
         "12sg68q1i8h2nzi4qf9izqb9qq89vvl9pzzi2nn353vsx0a4m0c3"))))
    (properties `((upstream-name . "RMassBank")))
    (build-system r-build-system)
    (inputs `(("openbabel" ,openbabel)))
    (propagated-inputs
     `(("r-biobase" ,r-biobase)
       ("r-envipat" ,r-envipat)
       ("r-digest" ,r-digest)
       ("r-httr" ,r-httr)
       ("r-msnbase" ,r-msnbase)
       ("r-mzr" ,r-mzr)
       ("r-cdk" ,r-cdk)
       ("r-rcpp" ,r-rcpp)
       ("r-rcurl" ,r-rcurl)
       ("r-rjson" ,r-rjson)
       ("r-s4vectors" ,r-s4vectors)
       ("r-xml" ,r-xml)
       ("r-markdown" ,r-markdown)
       ("r-knitr" ,r-knitr)
       ("r-yaml" ,r-yaml)))
    (home-page
     "https://bioconductor.org/packages/release/bioc/html/RMassBank.html")
    (synopsis
     "Workflow to process tandem MS files and build MassBank records")
    (description
     "Workflow to process tandem MS files and build MassBank records.
Functions include automated extraction of tandem MS spectra, formula
assignment to tandem MS fragments, recalibration of tandem MS spectra with
assigned fragments, spectrum cleanup, automated retrieval of compound
information from Internet databases, and export to MassBank records.")
    (license license:artistic2.0)))

(define-public r-massbank
  (package (inherit r-massbank-2.14.1)
	   (version "2.99.2")
	   (source
	    (origin
	      (method git-fetch)
	      (uri (git-reference (url "https://github.com/MassBank/RMassBank.git")
				  (commit "611b78578b54156119080b57569c09586a18fe84")))
	      (sha256
               (base32
                "07k56g14s23l7w3xsp840i1kym00i45l41vcwsyl3wd5ylfy87id"))))))

(define-public r-envipat
  (package
    (name "r-envipat")
    (version "2.4")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "enviPat" version))
        (sha256
          (base32
            "1gvb7jmwwh4l44b50xmrq3bysr2iv6442yh9pdk6n81g3bgpz1d0"))))
    (properties `((upstream-name . "enviPat")))
    (build-system r-build-system)
    (home-page "https://www.envipat.eawag.ch")
    (synopsis
      "Isotope Pattern, Profile and Centroid Calculation for Mass Spectrometry")
    (description
      "Fast and very memory-efficient calculation of isotope patterns,
subsequent convolution to theoretical envelopes (profiles) plus valley
detection and centroidization or intensoid calculation.  Batch
processing, resolution interpolation, wrapper, adduct calculations and
molecular formula parsing.  Loos, M., Gerber, C., Corona, F.,
Hollender, J., Singer, H. (2015) <doi:10.1021/acs.analchem.5b00941>.")
    (license license:gpl2)))


(define-public r-orgmassspecr
  (package
    (name "r-orgmassspecr")
    (version "0.5-3")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "OrgMassSpecR" version))
        (sha256
          (base32
            "1dx9d8rb1dfqyhyc26zhfnxiv3rz2ikvs2mwqnsrq3lsjs9dvyc8"))))
    (properties `((upstream-name . "OrgMassSpecR")))
    (build-system r-build-system)
    (home-page "http://OrgMassSpec.github.io/")
    (synopsis "Organic Mass Spectrometry")
    (description
      "Organic/biological mass spectrometry data analysis.")
    (license license:bsd-2)))



(define-public r-nontargetdata
  (package
    (name "r-nontargetdata")
    (version "1.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url  "https://github.com/blosloos/nontargetdata.git")
	     (commit "75b2a92cd549f72cc71dfe7b86bd42947d62f2c4")))
       (sha256
        (base32
         "1ax2lhg9v4k2fv1c8bh9zcyh7w2dpgy81bkds5aaziffwvaaxrak"))))
    (build-system r-build-system)
    (home-page
     "https://github.com/blosloos/nontargetdata")
    (synopsis
     "Data sets for nontarget package")
    (description
     "Data sets for isotope pattern grouping of LC-HRMS peaks with
package nontarget. Based on a vast set of unique PubChem molecular
formulas, quantized m/z, m/z differences, intensity ratios and marker
centroids of simulated centroid pairs are listed for different
instrument resolutions.")
    (license license:gpl3)))

(define-public r-nontarget
  (package
    (name "r-nontarget")
    (version "2.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url  "https://github.com/blosloos/nontarget.git")
	     (commit "3de1153e5aa6082e07f8c533545428e855bb5f6a")))
       (sha256
        (base32
         "1mghks0ars4cgy1vvnrg5vlqsn2zji6jn6qdbb4r2xbcwfm1dsk8"))))
    (build-system r-build-system)
    (propagated-inputs
     `(("r-envipat" ,r-envipat)
       ("r-mgcv" ,r-mgcv)
       ("r-nontargetdata" ,r-nontargetdata)))
    (home-page
     "https://github.com/blosloos/nontarget")
    (synopsis
     "Detecting Isotope, Adduct and Homologue Relations in LC-MS Data.")
    (description
     "Grouping of peaks in a HRMS data set for isotopic pattern
relations and different adducts of the same molecule; detection of
homologue series.  Isotopic pattern and adduct groups can then be
related to their (unknown) candidate chemical component, with
homologue series information attached.  Includes various plotting and
filtering functions for e.g. mass defects, frequent m/z distances,
components vs. non-components, adduct frequencies.  }")
    (license license:gpl3)))


(define-public r-chemmass
  (package
    (name "r-chemmass")
    (version "0.1.23")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/schymane/RChemMass.git")
	     (commit "5ed108f9f554247ce303a6b39ae5caf3569d1c89")))
       (sha256
        (base32
         "15pc6p6xv9n8vjfpkd4sgnx4s10w5z4b03bgbx20l490q2m51pxl"))))
    (build-system r-build-system)
    (propagated-inputs
     `(("r-massbank" ,r-massbank)
       ("r-cdk" ,r-cdk)
       ("r-cdklibs" ,r-cdklibs)
       ("r-envipat" ,r-envipat)
       ("r-curl" ,r-curl)
       ("r-svg" ,r-svg)))
    (home-page
     "https://github.com/schymane/ReSOLUTION")
    (synopsis
     "SOLUTIONS for High ReSOLUTION Mass Spectrometry")
    (description
     "This package provides workflow functionality for non-target
screening with high resolution mass spectrometry and many functions
for interacting with MetFrag.")
    (license license:artistic2.0)))

(define-public r-resolution
  (package
    (name "r-resolution")
    (version "0.1.8")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/schymane/ReSOLUTION.git")
	     (commit "cf30ffe77656360442004931d02c3126e7adb262")))
       (sha256
        (base32
         "0hrniilnknzwqfab76h2h64gmmjgxwmhcn9h28p5cpbnryrayfyw"))))
    (build-system r-build-system)
    (propagated-inputs
     `(("r-massbank" ,r-massbank)
       ("r-chemmass" ,r-chemmass)
       ("r-orgmassspecr" ,r-orgmassspecr)
       ("r-nontarget" ,r-nontarget)
       ("r-cdk" ,r-cdk)
       ("r-cdklibs" ,r-cdklibs)
       ("r-envipat" ,r-envipat)
       ("r-readxl" ,r-readxl)
       ("r-curl" ,r-curl)
       ("r-svg" ,r-svg)))
    (home-page
     "https://github.com/schymane/ReSOLUTION")
    (synopsis
     "SOLUTIONS for High ReSOLUTION Mass Spectrometry")
    (description
     "This package provides workflow functionality for non-target
screening with high resolution mass spectrometry and many functions
for interacting with MetFrag.")
    (license license:artistic2.0)))
