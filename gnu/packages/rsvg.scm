(define-module (gnu packages rsvg)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system r)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages chemistry)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages web)
  #:use-module (gnu packages image)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages))

(define-public r-svg
  (package
    (name "r-svg")
    (version "1.3")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "rsvg" version))
        (sha256
          (base32
            "11mccgf6hfskg45wqc114sx3qy2r494y6axdf73z6xwhs1wpm97g"))))
    (build-system r-build-system)
    (inputs `(("librsvg" ,librsvg) ("zlib" ,zlib)))
    (native-inputs `(("pkg-config" ,pkg-config)))
    (home-page
      "https://github.com/jeroen/rsvg#readme")
    (synopsis
      "Render SVG Images into PDF, PNG, PostScript, or Bitmap Arrays")
    (description
      "Renders vector-based svg images into high-quality custom-size bitmap arrays using 'librsvg2'.  The resulting bitmap can be written to e.g.  png, jpeg or webp format.  In addition, the package can convert images directly to various formats such as pdf or postscript.")
    (license  license:expat)))

